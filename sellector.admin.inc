<?php

/**
 *	Sellector administration form
 */
 
function sellector_settings_form() {
	$form = array();
	
	$form['sellector_desc'] = array(
		'#value'		=>	t('Use this form to configure your received Sellector.com ID and the width and height of the selection and result area. After that, please visit the block configuration page to position the configured areas.'),							
	);
	
	$form['sellector_id'] = array(
		'#type'			=>	'fieldset',
		'#title'		=>	t('Sellector.com ID'),
		'#collapsible'	=>	TRUE,
	);
	
	$form['sellector_id']['sell_id'] = array(
		'#type'			=>	'textfield',
		'#title'		=>	t('Sellector ID'),
		'#size'			=>	20,
		'#required'		=>	TRUE,
		'#default_value'=>	variable_get('sellector_id', ''),
	);
	
	$form['result_area'] = array(
		'#type'			=>	'fieldset',
		'#title'		=>	t('Result area'),
		'#collapsible'	=>	TRUE,
	);
	
	$form['result_area']['result_width'] = array(
		'#type'			=>	'textfield',
		'#title'		=>	t('Width'),
		'#size'			=>	10,
		'#field_suffix'	=>	'px',
		'#required'		=>	TRUE,
		'#default_value'=>	variable_get('sellector_result_width', '600'),
	);
	
	$form['result_area']['result_height'] = array(
		'#type'			=>	'textfield',
		'#title'		=>	t('Height'),
		'#size'			=>	10,
		'#field_suffix'	=>	'px',
		'#required'		=>	TRUE,
		'#default_value'=>	variable_get('sellector_result_height', '600'),
	);
	
	$form['selection_area'] = array(
		'#type'			=>	'fieldset',
		'#title'		=>	t('Selection area'),
		'#collapsible'	=>	TRUE,
	);
	
	$form['selection_area']['select_width'] = array(
		'#type'			=>	'textfield',
		'#title'		=>	t('Width'),
		'#size'			=>	10,
		'#field_suffix'	=>	'px',
		'#required'		=>	TRUE,
		'#default_value'=>	variable_get('sellector_selection_width', '600'),
	);
	
	$form['selection_area']['select_height'] = array(
		'#type'			=>	'textfield',
		'#title'		=>	t('Height'),
		'#size'			=>	10,
		'#field_suffix'	=>	'px',
		'#required'		=>	TRUE,
		'#default_value'=>	variable_get('sellector_selection_height', '600'),
	);
	
	$form['submit'] = array(
		'#type'			=>	'submit',
		'#value'		=>	t('Save'),
	);
	
	return $form;
}

function sellector_settings_form_submit($form_id, $values) {
	variable_set('sellector_id', $values['values']['sell_id']);
	variable_set('sellector_result_width', $values['values']['result_width']);
	variable_set('sellector_result_height', $values['values']['result_height']);
	variable_set('sellector_selection_width', $values['values']['select_width']);
	variable_set('sellector_selection_height', $values['values']['select_height']);
	return drupal_set_message(t('Configuration saved!'));
}

?>